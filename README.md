# The MPI Bugs Initiative

The MPI Bugs Initiative [MBI21](https://hal.archives-ouvertes.fr/hal-03474762) is a collection of MPI codes that aims at assessing the status of MPI verification tools.

## Visualization Dashboard

You can find our results on the [Visualization dashboard](https://mpibugsinitiative.gitlab.io/MpiBugsInitiative/)
Note that these results correspond to MBI 1.0.0.  

## Quick Start

Docker is used to facilitate the benchmark modifications and the installation of the tools we have selected.
You can create a docker image containing all dependencies needed to evaluate the tools using the provided Dockerfile.
The following commands will create and run the image:
```bash
docker build -f Dockerfile -t mbi:latest .
docker run -it mbi bash 
```

Once inside the docker, the scripts you need are in /MBI/scripts/tools. One script is provided for each tool.
The result can then be explored under /MBI/logs/{tool} (a new such directory is created each time you launch a tool)). Note that some tools are already installed in MBI-builds/. 
Three files are created per test: 
- {test_name}.txt that contains the output of the test 
- {test_name}.elapsed that gives the time of the test
- {test_name}.md5sum which is the cache

A test is launched if {test_name}.txt or {test_name}.elapsed do not exist or if {test_name} has been modified ({test_name}.md5sum has changed).

Command to generate all c codes:
```bash
python3 MBI.py -c generate
```

You can launch all tests outside the docker image by using
```bash
./test-all
```

To test a specific tool:
1. Build the docker container: 
```bash 
docker build -f Dockerfile -t mbi:latest .
 ```
2. Run the container: 
```bash
docker run -v `pwd`:/MBI -it mbi bash
 ```
3. Generate codes: 
```bash
python3 ./MBI/MBI.py -c generate
```
4. Build and run a tool: 
```bash
python3 ./MBI/MBI.py -x (tool) -c run 
```
5. Get statistics on a tool:
```bash
python3 ./MBI/MBI.py -x (tool) -c html 
```

## Codes Information

All programs in the benchmark have a formated header with the following feature and error labels.
We define 7 feature labels and 9 types of errors. Each label is either set to Yes: use of the feature, or
Lacking: the feature is missing.


MPI Feature Label | Description 
 -----------------|--------------------
 P2P:base calls | Use of blocking point-to-point communication  
 P2P:nonblocking  | Use of nonblocking point-to-point communication 
 P2P:persistent | Use of persistent communication 
 COLL:base calls  | Use of blocking collective communication  
 COLL:nonblocking | Use of nonblocking collective communication 
 COLL:tools| Use of resource function (e.g., communicators, datatypes) 
 RMA   | Use of remote memory access (RMA)  


Scope | Error Label |  Description
 -----|-------------|--------------------
 Single call | Invalid Parameter | Invalid parameter in a MPI function (e.g., invalid root, communicator)
 Single process | Resource Leak | Resource leaking (e.g., communicator not freed)
 Single process | Request Lifecycle | Missing wait or start function
 Single process  | Epoch Lifecycle | Missing epoch function
 Single process  | Local Concurrency | Data race in a process
 Multi process  | Parameter Matching | Wrong matching of parameters
  Multi process  | Message Race | Non-determinism execution, caused by the use of ANY_SRC
 Multi process  | Call Ordering | Call mismatch
   Multi process   | Global Concurrency | Data race resulting from multiple processes


## Tools Information

We use the MBI to compare Aislinn, CIVL, ISP, ITAC, McSimGrid, MPI-SV, MUST and PARCOACH.


Tool | Version | Compiler 
-----|---------|---------
Aislinn | v3.12 | GCC 7.4.0
CIVL | v1.20 | used with JDK-14
ISP | 0.3.1 | GCC 10.2.0
MUST | v1.7 | GCC 10.2.0
PARCOACH | v3.0 | LLVM 15
McSimGrid | v3.27 |  GCC 10.2.0
ITAC | 2021.3 | 
MPI-SV | v1 | GCC 4.8.4


## Metrics used

- Results
   - TP = true positive
   - TN = true negative
   - FP = false positive
   - FN = false negative
- Errors
   - CE = compilation error
   - RE = runtime error
   - TO = time out
- Total = CE+RE+TO+TP+FP+TN+FN
- Robustness
   - Coverage: Cov = 1 - (CE/Total)
   - Conclusiveness: Cc = 1 - ((CE+RE+TO)/Total)
- Usefulness
   - Specificity: S = TN / ( TN + FP)
   - Recall: R = TP / ( TP + FN)
   - Precision: P = TP / ( TP + FP)
   - F1 Score: F1 = (2 * P * R) / (P + R)
- Overall accuracy: A = (TP+TN) / Total

